import React, { useEffect, useState } from 'react'
import cn from 'classnames'
import ShareButton from './share-button'
import styles from './styles/footer.css'

const title = 'Shortstories. Стань лучшим автором'

function Footer({ className = '' }) {
  const [origin, setOrigin] = useState(null)
  useEffect(() => {
    setOrigin(window.location.origin)
  }, [origin])
  return (
    <footer className={cn(styles.footer, className)}>
      <div className={styles.bar}>
        <div className={styles.copyright}>
          <div>
            Канал о проекте:{' '}
            <a
              href="tg://resolve?domain=fash_it"
              target="_blank"
              rel="noopener noreferrer"
            >
              ФЭШ
            </a>
          </div>
        </div>
        <div className={styles.share}>
          <ShareButton
            href="https://vk.com/shortstories_io"
            title="Группа ВК"
            icon="/static/images/icons/vk.svg"
          />
          <ShareButton
            href={`https://twitter.com/intent/tweet/?text=${title}&url=${origin}`}
            title="Поделиться в Twitter"
            icon="/static/images/icons/twitter.svg"
          />
          <ShareButton
            href={`https://telegram.me/share/url?url=${origin}&text=${title}&utm_source=share2`}
            title="Поделиться в Telegram"
            icon="/static/images/icons/telegram.svg"
          />
          <ShareButton
            href={`https://www.facebook.com/sharer/sharer.php?u=${origin}&t=${title}`}
            title="Поделиться в Facebook"
            icon="/static/images/icons/fb.svg"
          />
        </div>
      </div>
    </footer>
  )
}

export default Footer
